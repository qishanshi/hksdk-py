import hk_dll
import hk_class
from ctypes import *

#用户登录句柄
lUserID = -1
#设备信息
m_strDeviceInfo = None

SERIALNO_LEN = 48  # 序列号长度
NAME_LEN = 32  # 用户名长度py

#初始化
def init():
    return hk_dll.NET_DVR_Init()

#登录
def login(ip, port, username, password):
    global lUserID
    #注册之前先注销已注册的用户,预览情况下不可注销
    if lUserID > -1:
        #先注销
        hk_dll.NET_DVR_Logout_V30(lUserID)
    
    #注册
    m_strDeviceInfo = hk_class.NET_DVR_DEVICEINFO_V30()
    m_strDeviceInfo.sSerialNumber = (c_ubyte * SERIALNO_LEN)()
    m_strDeviceInfo.byRes1 = (c_ubyte * 24)()

    lUserID = hk_dll.NET_DVR_Login_V30(bytes(ip), port, bytes(username), bytes(password), m_strDeviceInfo)

    initSuc = hk_dll.NET_DVR_SetLogToFile(3, b'./sdklog', False)

    if lUserID == -1:
        print("登录失败,{}", lUserID)
        return False

    return True

def logout():
    hk_dll.NET_DVR_Logout_V30(lUserID)

def cleanup():
    hk_dll.NET_DVR_Cleanup()

def getTemperature(x, y, sourceWidth, sourceHeight):
    bRet = False
    nErr = 0
    global lUserID

    # m_struThermometryInfo = hk_class.NET_DVR_THERMOMETRY_PRESETINFO()
    # m_struThermometryInfo.byRes = (c_ubyte * 2)()
    
    # m_struThermometryInfo.struPresetInfo = (hk_class.NET_DVR_THERMOMETRY_PRESETINFO_PARAM * 40)()
    # m_struThermometryInfo.dwSize = sizeof(m_struThermometryInfo)

    # m_struThermometryCond = hk_class.NET_DVR_THERMOMETRY_COND()
    # m_struThermometryCond.dwSize = sizeof(m_struThermometryCond)
    # m_struThermometryCond.dwChannel = 1
    # m_struThermometryCond.wPresetNo = 1
    # m_struThermometryCond.byRes = (c_ubyte * 62)()

    # struCfg = hk_class.NET_DVR_STD_CONFIG()
    # struCfg.lpCondBuffer = pointer(m_struThermometryCond)
    # struCfg.dwCondSize = sizeof(m_struThermometryCond)
    # struCfg.lpOutBuffer = pointer(m_struThermometryInfo)
    # struCfg.dwOutSize = sizeof(m_struThermometryInfo)
    # struCfg.byRes = (c_ubyte * 23)()

    # m_szStatusBuf = hk_class.BYTE_ARRAY((c_byte * 16384)())
    # struCfg.lpStatusBuffer = pointer(m_szStatusBuf)
    # struCfg.dwStatusSize = 16384
    # struCfg.byDataType = 0

    # bRet = hk_dll.NET_DVR_GetSTDConfig(lUserID, 3624, struCfg)
    # if not bRet:
    #     nErr = hk_dll.NET_DVR_GetLastError()
    #     print("NET_DVR_GetSTDConfig失败", nErr)
    #     return 0

    # m_struThermometryInfo.dwSize = sizeof(m_struThermometryInfo)
    # m_struThermometryInfo.wPresetNo = 1
    
    # m_struThermometryInfo.struPresetInfo[0] = hk_class.NET_DVR_THERMOMETRY_PRESETINFO_PARAM(0,0,0,0.0,0,(c_ubyte * 2)(),0,0.0,(c_ubyte * NAME_LEN)(),(c_ubyte * 63)(),0,hk_class.NET_VCA_POINT(),hk_class.NET_VCA_POLYGON(0,(hk_class.NET_VCA_POINT * 10)()))
    # m_struThermometryInfo.struPresetInfo[0].byEnabled = 1
    # m_struThermometryInfo.struPresetInfo[0].byRuleID = 1
    # m_struThermometryInfo.struPresetInfo[0].wDistance = 10
    # m_struThermometryInfo.struPresetInfo[0].fEmissivity = 0.9599
    # m_struThermometryInfo.struPresetInfo[0].byReflectiveEnabled = 0
    # m_struThermometryInfo.struPresetInfo[0].fReflectiveTemperature = 20
    # m_struThermometryInfo.struPresetInfo[0].byRuleCalibType = 2
    # m_struThermometryInfo.struPresetInfo[0].byDistanceUnit = 0
    # m_struThermometryInfo.struPresetInfo[0].struPoint = hk_class.NET_VCA_POINT()
    # m_struThermometryInfo.struPresetInfo[0].struPoint.fX = 0
    # m_struThermometryInfo.struPresetInfo[0].struPoint.fY = 0
    # m_struThermometryInfo.struPresetInfo[0].struRegion = hk_class.NET_VCA_POLYGON(0,(hk_class.NET_VCA_POINT * 10)())
    # m_struThermometryInfo.struPresetInfo[0].struRegion.dwPointNum = 2
    # m_struThermometryInfo.struPresetInfo[0].struRegion.struPos[0] = hk_class.NET_VCA_POINT()
    # m_struThermometryInfo.struPresetInfo[0].struRegion.struPos[0].fX = 0.187
    # m_struThermometryInfo.struPresetInfo[0].struRegion.struPos[0].fY = 0.6119
    # m_struThermometryInfo.struPresetInfo[0].struRegion.struPos[1] = hk_class.NET_VCA_POINT()
    # m_struThermometryInfo.struPresetInfo[0].struRegion.struPos[1].fX = 0.876
    # m_struThermometryInfo.struPresetInfo[0].struRegion.struPos[1].fY = 0.569

    # struCfg.lpInBuffer = pointer(m_struThermometryInfo)
    # struCfg.dwInSize = sizeof(m_struThermometryInfo)

    # setSTDConfig = hk_dll.NET_DVR_SetSTDConfig(lUserID, 3625, struCfg)
    # print("NET_DVR_SetSTDConfig", setSTDConfig)

    m_strJpegWithAppenData = hk_class.NET_DVR_JPEGPICTURE_WITH_APPENDDATA()
    m_strJpegWithAppenData.dwSize = sizeof(m_strJpegWithAppenData)
    m_strJpegWithAppenData.dwChannel = 1
    m_strJpegWithAppenData.pJpegPicBuff = pointer(hk_class.BYTE_ARRAY1((c_byte * 2097152)()))
    m_strJpegWithAppenData.pP2PDataBuff = pointer(hk_class.BYTE_ARRAY2((c_byte * 2097152)()))
    m_strJpegWithAppenData.byRes = (c_ubyte * 255)()

    bRet = hk_dll.NET_DVR_CaptureJPEGPicture_WithAppendData(lUserID, 2, m_strJpegWithAppenData)
    print('bRet',bRet)
    if bRet:
        #测温数据
        print('m_strJpegWithAppenData.dwP2PDataLen',m_strJpegWithAppenData.dwP2PDataLen)
        if m_strJpegWithAppenData.dwP2PDataLen > 0:
            x , y = point2point(x , y, sourceWidth, sourceHeight, m_strJpegWithAppenData.dwJpegPicWidth, m_strJpegWithAppenData.dwJpegPicHeight)
            getTemperature0(m_strJpegWithAppenData.pP2PDataBuff)

    return 0

def getTemperature0(pP2PDataBuff):
    print('pP2PDataBuff',pP2PDataBuff.value)
    # byTempData = c_ubyte * 4
    # byteBuffer.get(byTempData)

    # int l = byTempData[0]
    # l &= 0xff
    # l |= ((long) byTempData[1] << 8)
    # l &= 0xffff
    # l |= ((long) byTempData[2] << 16)
    # l &= 0xffffff
    # l |= ((long) byTempData[3] << 24)

    # return Float.intBitsToFloat(l)

def point2point(x, y, sourceWidth, sourceHeight, targetWidth, targetHeight):
    x = x * sourceWidth / targetWidth
    y = y * sourceHeight / targetHeight

    x = x if x >= targetWidth else targetWidth
    x = 0 if x < 0 else x

    y = y if y >= targetHeight else targetHeight
    y = 0 if y < 0 else y

    return x,y