import hk_sdk as hk_sdk
import hk_class as hkclass
import time

def test():
    result = hk_sdk.init()
    if not result:
        print('初始化失败,错误码是: ', hk_sdk.get_last_error())
        return False

    user_id = hk_sdk.login(b"192.168.8.16", 8000, b'admin', b'a1234567')
    if user_id == -1:
        print('登录失败,错误码是: ', hk_sdk.get_last_error())
        return False

    #成像通道号
    channel_no = 2
    # 测试获取(100, 100)点的温度10次，1280 720 分别是原始图片长宽
    for i in range(10):
        result, temperature = hk_sdk.get_temperature(100, 100, 1280, 720, user_id, channel_no)
        
        if result:
            print("获取的温度是", temperature, i)
        else:
            print("获取温度失败 ", temperature, "错误码是: ", hk_sdk.get_last_error())
            return False
        
        time.sleep(1)

    # 退出登录
    hk_sdk.logout(user_id)

    # 释放
    hk_sdk.cleanup()

if __name__ == "__main__":
    test()
