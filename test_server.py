
# !/usr/bin/python3
# encoding:utf8
# author:rhq
# datetime: 2020.08.19

"""
编写项目视图脚本
"""

from flask import Flask, request, abort,  jsonify
import hk_sdk as hs
import hk_dll as hk_dll


app = Flask(__name__)


@app.route('/hksdk_init', methods=["POST"])
def hksdk_init():
    print(1111111111111)
    if not request.json:
        abort(400)
    else:
        print(request.json)
        # url = request.json["url"]
    if hs.init():
        # result = url.split("//")
        # result = result[1].split(":")
        # username = result[0]
        # result = result[1].split("@")
        # password = result[0]
        # ip = result[1]
        if hs.login(b"192.168.8.14", 8000, b"admin", b"a1234567"):
            print("sdk lofing 555")
            return jsonify({"status": True, "data": None, "message": "login successful!", "code": 200})
        else:
            return jsonify({"status": False, "data": None, "message": "login error!", "code": 200})


@app.route('/get_point_value', methods=["POST"])
def get_point_value():
    # print(888888888)
    if not request.json:
        abort(400)
    else:
        # print(request.json)
        result, temperature = hs.get_temperature_max(
            request.json["point_list"], request.json["yw"], request.json["yh"])
        print('获取温度信息:', result, temperature)
        if result:
            return jsonify({"status": True, "data": temperature, "message": "get successful", "code": 200})
        else:
            return jsonify({"status": False, "data": None, "message": "get error!", "code": 200})
    return jsonify({"status": False, "data": None, "message": "SDK init error!", "code": 200})


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5555, debug=True)
