# coding=utf-8

import hk_dll as dll
import hk_class as hkclass
import hk_sdk as sdk
from ctypes import sizeof, addressof, memmove, c_ubyte, c_void_p, POINTER, pointer
import time

#全局变量
struThermometry = hkclass.NET_DVR_THERMOMETRY_UPLOAD()

# GetThermInfoCallback(DWORD dwType, void* lpBuffer, DWORD dwBufLen, void* pUserData)
def fRemoteConfigCallback(dwType, lpBuffer, dwBufLen, pUserData):
    print("call back!")
    print(dwType)
    #memmove(addressof(struThermometry), lpBuffer, sizeof(struThermometry));

if __name__ == "__main__":

    #sdk初始化
    result = dll.NET_DVR_Init()

    #设置连接时间与重连时间
    dll.hCNetSDK.NET_DVR_SetConnectTime(2000, 1)
    dll.hCNetSDK.NET_DVR_SetReconnect(10000, True)
    
    if result:
        print('初始化成功')

        #登录sdk
        lUserID = sdk.login(b"192.168.8.204", 8000, b'admin', b'a1234567')

        if lUserID < 0:
            print("Login failed, error code: %d\n", dll.hCNetSDK.NET_DVR_GetLastError());
            dll.NET_DVR_Cleanup()
            exit(-1)
        else:
            print("login success! ", lUserID)
    
        #启动实时温度检测
        struThermCond = hkclass.NET_DVR_REALTIME_THERMOMETRY_COND()
        struThermCond.byRuleID = 1 #规则ID，0代表获取全部规则，具体规则ID从1开始
        struThermCond.dwChan = 2 #从1开始，0xffffffff代表获取全部通道
        struThermCond.byRes = (c_ubyte * 56)()
        struThermCond.dwSize = sizeof(struThermCond)

        cbStateCallback = dll.GetThermInfoCallback(fRemoteConfigCallback)
        lHandle = dll.hCNetSDK.NET_DVR_StartRemoteConfig(lUserID, hkclass.NET_DVR_GET_REALTIME_THERMOMETRY, pointer(struThermCond), sizeof(struThermCond), cbStateCallback, None)
        

    if lHandle < 0:
        print("NET_DVR_GET_REALTIME_THERMOMETRY failed, error code: %d\n", dll.hCNetSDK.NET_DVR_GetLastError())
    else:
        print("NET_DVR_GET_REALTIME_THERMOMETRY is successful!")

    value = ""
    while "q" != value:
        print("wait...")
        value = input('input q for exit: ')
    

    #关闭长连接配置接口所创建的句柄，释放资源
    if dll.hCNetSDK.NET_DVR_StopRemoteConfig(lHandle):
        print("NET_DVR_StopRemoteConfig failed, error code: %d\n", dll.hCNetSDK.NET_DVR_GetLastError())

    #注销用户，如果前面没有登录，该步骤则不需要
    dll.NET_DVR_Logout_V30(lUserID)

    #释放sdk
    dll.NET_DVR_Cleanup()
            
